#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias config='/usr/bin/git --git-dir=/home/matt/gitlab/dotfiles --work-tree=/home/matt'
neofetch
alias vi='nvim'
alias telegram-cli='telegram-cli -N'

if [[ -d "$HOME/bin" ]]; then
	export PATH=$PATH:$HOME/bin
fi
