# dotfiles

Personal config files 

Modified qtile config from https://gitlab.com/dwt1/dotfiles for dual monitor setup. Also removed some extra widgets I don't need.

## packages
  * mpd
  * ncmpcpp
  * neovim
  * [nerd-fonts](https://aur.archlinux.org/packages/nerd-fonts-complete)
  * pywal
  * qutebrowser
  * qtile
  * rofi
  * weechat
  * [unclutter](https://aur.archlinux.org/packages/unclutter-xfixes-git)
  * urxvt
  * urxvt-perls

